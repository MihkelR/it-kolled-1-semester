package kolled�;


public class Mang {

	public static void main(String[] args) {
		System.out.println("Tere tulemast IT Kolled�isse!!!!");
		System.out.println("Liikumiseks on (w,a,s,d), kui soovid abi truki (h).");
		String[][] maastik = {
				{"|", "-", "-", "-", "-", "-", "-", "-", "|"},
				{"|", "R", "|", " ", "?", " ", " ", "?", "|"},
				{"|", " ", "|", "?", "|", " ", "|", " ", "|"},
				{"|", "?", "|", " ", "|", "?", "|", "S", "|"},
				{"|", " ", "|", "S", "|", " ", "|", "?", "|"},
				{"|", "?", " ", " ", "|", " ", " ", " ", "|"},
				{"|", "-", "-", "-", "-", "-", " ", " ", "|"},
				{"|", "x", " ", "?", " ", "?", " ", "S", "|"},
				{"|", "-", "-", "-", "-", "-", "-", "-", "|"}
		};
		
		looMaastik(maastik);
		tabel(maastik, 7, 1);
	}
	//Otsib maatriksist kohad, milles on ? ja taidab need alumises meetodis stringidega.
	public static String[][] looMaastik(String[][] maastik) {
		for (int i = 0; i < maastik.length; i++) {
			for (int j = 0; j < maastik.length; j++) {
				if (maastik[i][j].equals("?")) {
					maastik[i][j] = taht();
				}
			}
		}
		return (maastik);
	}
	//Suvaliselt valib Stringe
	public static String taht() {
		int suva;
		suva = (int) (Math.random() * 6);
		if (suva == 0) {
			return ("M");
		} else if (suva == 1) {
			return ("I");
		} else if (suva == 2) {
			return ("F");
		} else if (suva == 3) {
			return ("O");
		} else if (suva == 4) {
			return ("J");
		} else {
			return ("6");
		}
	}
	
	// Kirjutab uhe rea.
	public static void uksRida(String[] maastik) {
		for (int i = 0; i < maastik.length; i++) {
			System.out.print(maastik[i] + " ");
		}
	}

	// Genereerib manguvaljakut.
	public static void tabel(String[][] maastik, int x, int y) {
		for (int i = 0; i < maastik.length; i++) {
			uksRida(maastik[i]);
			System.out.println();
		}
		yourMove(maastik, x, y);
	}

	// Vaatab, mis kaigu sa tegid ja kaitub vastavalt.
	public static void yourMove(String[][] maastik, int x, int y) {
		System.out.println("Kuhu kaia soovid?");
		String move = TextIO.getlnString();

		if (move.equals("w")) {
			if (kontroll(maastik, x - 1, y)) {
				maastik[x - 1][y] = "x";
				maastik[x][y] = " ";
				x = x - 1;
			} else {
				yourMove(maastik, x, y);
			}
		} else if (move.equals("s")) {
			if (kontroll(maastik, x + 1, y)) {
				maastik[x + 1][y] = "x";
				maastik[x][y] = " ";
				x = x + 1;
			} else {
				yourMove(maastik, x, y);
			}
		} else if (move.equals("a")) {
			if (kontroll(maastik, x, y - 1)) {
				maastik[x][y - 1] = "x";
				maastik[x][y] = " ";
				y = y - 1;
			} else {
				yourMove(maastik, x, y);
			}
		} else if (move.equals("d")) {
			if (kontroll(maastik, x, y + 1)) {
				maastik[x][y + 1] = "x";
				maastik[x][y] = " ";
				y = y + 1;
			} else {
				yourMove(maastik, x, y);
			}
		} else if (move.equals("h")) {
			OppeAined.abi();
		} else if (move.equals("v")) {
			OppeAined.varud();
		} else {
			System.out
					.println("Kui sa ei tea mida sa teed, vajuta h ja enter.");
			yourMove(maastik, x, y);
		}

		System.out.println("---------------------------------");
		tabel(maastik, x, y);
	}

	// Kontroll, kuhu sa kaid ja viib sind vastavasse kohta.
	public static boolean kontroll(String[][] maastik, int x, int y) {
		if (maastik[x][y].equals("-")) {
			System.out.println("Sorry, sinna ei saa kaia, see on sein.");
			return false;
		} else if (maastik[x][y].equals("|")) {
			System.out.println("Sorry, sinna ei saa kaia, see on sein.");
			return false;
		}else if (maastik[x][y].equals("I")) {
			OppeAined.sissejuhatusInformaatikasse();
			return true;
		}else if (maastik[x][y].equals("O")) {
			OppeAined.makrookonoomika();
			return true;
		}else if (maastik[x][y].equals("F")) {
			OppeAined.fuusika();
			return true;	
		}else if (maastik[x][y].equals("J")) {
			OppeAined.java();
			return true;
		}else if (maastik[x][y].equals("6")) {
			OppeAined.opingukorraldus();
			return true;
		}else if (maastik[x][y].equals("M")) {
			OppeAined.makrookonoomika();
			return true;
		}else if (maastik[x][y].equals("S")) {
			OppeAined.sober();
			return true;
		}else if (maastik[x][y].equals("R")) {
			OppeAined.mangLabi();
			return true;
		} else {
			return true;
		}
	}
	
	
}