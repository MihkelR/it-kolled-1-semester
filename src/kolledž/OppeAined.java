package kolled�;

public class OppeAined {
	static int punkte;
	static int sober;
	
	//prindib valja punktide ja soprade arvu.
	public static void varud() {
		System.out.println("Punkte on sul: " + punkte);
		System.out.println("Sopru on sul " + sober);
		System.out.println("Truki 'edasi', kui tahad mangimist jatkata.");

		String valmis = TextIO.getlnString();
		if (valmis.equals("edasi")) {
			return;
		} else {
			System.out.println("Soovid veel lugeda? Aga palun.");
			System.out.println();
			varud();
		}
	}
	//Trukib valja mangu kohta informatsiooni
	public static void abi() {
		System.out.println();
		System.out.println("Mang kujutab endast esimest semestrit IT Kolled�-is.\nSa liigud ringi w,a,s,d nuppudega ja oma hetkelisi sopru ning punkte saad naha v klahvi vajutades.");
		System.out.println("Sa pead valikvastustega kusimustele vastama, et punkte koguda ja semester ara teha\n Kui kogud -4 punkti kukud valja. 12 punkti on maksimum ja sul on vahemalt 8 vaja, et semester labida.");
		System.out.println("X-sina\nS-sober, keda saad kasutada kusimuse ajal, ta aitab sind.");
		System.out.println("M-Matemaatiline analuus\nI-Sissejuhatus informaatikasse\nF-Fuusika");
		System.out.println("O-Makrookonoomika\nJ-Programmeerimise algkursus Java baasil\n6-Opingukorraldus ja erialatutvustus");
		System.out.println("Truki 'edasi', kui tahad mangimist jatkata.");

		String valmis = TextIO.getlnString();
		if (valmis.equals("edasi")) {
			return;
		} else {
			System.out.println("Soovid veel lugeda? Aga palun.");
			System.out.println();
			abi();
		}

	}
	//Jargnevalt tuleb 6 oppeainet, igas aines on neli kusimust, mis valitakse suvaliselt kui
	//selle aine tahe peale minnakse.
	public static void makrookonoomika() {
		System.out.println("Makrookonoomika!");
		int suva;
		suva = (int) (Math.random() * 4);
		if (suva == 0) {
			System.out.println("Disinflatsioon on:");
			System.out.println("a. vastandsundmus inflatsioonile\nb. inflatsioonitempo aeglustumine\nc. inflatsioonitempo jarsk kiirenemine\nd. toopuuduse ja inflatsiooni uheaegne esinemine");
			uldine("b", "a. ja b.");
			
		}
		
		if (suva == 1) {
			System.out.println("Stagflatsioon on:");
			System.out.println("a. hinnatousu ja tooviljakuse uheaegne muutus\nb. inflatsiooni ja toopuuduse uheaegne kasv\nc. inflatsiooni ja toopuuduse uheaegne langus\nd. majanduse taandareng");
			uldine("b", "b. ja d.");
			
		}
		if (suva == 2) {
			System.out.println("Pakkumisseadus vaidab");
			System.out.println("a. pakutud hobuse suhu ei vaadata\nb. pakkuda tuleb ainult kvaliteetset kauppa\nc. kauba hinna langemisel touseb pakutav kogus\nd. kauba hinna tousmisel touseb ka pakutav kogus");
			uldine("d", "a. ja d.");
			
		}
		if (suva == 3) {
			System.out.println("Millist seost iseloomustab tarbimisfunktsioon?");
			System.out.println("a. tarbimise ja saastmise vahel\nb. tarbimise ja SKP vahel\nc. kulutamise ja saastmise vahel\nd. tarbimise ja autonoomsete kulutuste vahel");
			uldine("b", "b. ja c.");
		}
	}
	
	public static void sissejuhatusInformaatikasse() {
		System.out.println("Sissejuhatus Informaatikasse!");
		int suva;
		suva = (int) (Math.random() * 4);
		if (suva == 0) {
			System.out.println("Millistel muutuja vaartustel on lause (Av(B&A))v(-A&(Cv(B&-C))) vaar?");
			System.out.println("a. A=0, B=0 ja C=0\nb. A=0, B=1 ja C=1\nc. A=0, B=1 ja C=0\nd. A=1, B=0 ja C=0");
			uldine("a", "a. ja c.");	
		}
		if (suva == 1) {
			System.out.println("Mis on BIOS?");
			System.out.println("a. Basic Input/Output System\nb. Bootable Initial Operating System\nc. Bridged Interface On System");
			uldine("a", "a. ja b.");			
		}
		if (suva == 2) {
			System.out.println("Milline allolevatest HTML'i tagidest defineerib tabeli valja?");
			System.out.println("a. td\nb. tr\nc. th\nd. tc");
			uldine("a", "a. ja d.");	
		}
		if (suva == 3) {
			System.out.println("Kas on voimalik luua ainult AND ja OR gate-i kasutades komponent, mis suvalise sisendi peale ei valjastaks signaali?");
			System.out.println("a. Ei\nb. Jah");
			uldine("b", "a. ja b. , ega sa kahest 50/50 ei saa votta");
			
		}
	}

	public static void fuusika() {
		System.out.println("Fuusika!");
		int suva;
		suva = (int) (Math.random() * 4);
		if (suva == 0) {
			System.out.println("A tostab 20 kg 10 korda 1 min jooksul, B teeb seda 2 min jooksul");
			System.out.println("a. A voimsus on suurem \nb. B voimsus on suurem\nc. Molema voimsus on sama");
			uldine("a", "a. ja b.");
		}
		if (suva == 1) {
			System.out.println("Kui keha mass kahekordistub ja k�rgus jaab samaks, siis Epot");
			System.out.println("a. kasvab 4x \nb. Jaab samaks\nc. kasvab 2x");
			uldine("c", "b. ja c.");
		}
		if (suva == 2) {
			System.out.println("Kui keha mass vaheneb kaks korda ja kiirus kahekordistub, siis Ekin");
			System.out.println("a. kasvab 4 korda\nb. kasvab kaks korda\nc. jaab samaks");
			uldine("b", "a. ja b.");
		}
		if (suva == 3) {
			System.out.println("Mees surub taiest joust paneeli, et seda pusti hoida. Mees");
			System.out.println("a. teeb positiivset tood\nb. teeb negatiivset tood\nc. ei tee tood");
			uldine("b", "b. ja c.");
		}

	}

	public static void java() {
		System.out.println("Java!");
		int suva;
		suva = (int) (Math.random() * 4);
		if (suva == 0) {
			System.out.println("Milline jarnevatest tuupidest ei ole algtuup (primitive type)?");
			System.out.println("a. String\nb. int\nc. boolean\nd. double");
			uldine("a", "a. ja c.");
			
		}
		
		if (suva == 1) {
			System.out.println("Object-Oriented Programming means");
			System.out.println("a. Being objective about what you develop\nb. Designing the application based on the objects discovered when analysing the problem\nc. Writing and algorithm before writing your program and having a test plan\nd. Writing a program composed of Java classes");
			uldine("b", "b. ja d.");
			
		}
		if (suva == 2) {
			System.out.println("We can have any number of classes in a Java source file.");
			System.out.println("a. False\nb. True");
			uldine("b", "a. ja b.");
			
		}
		if (suva == 3) {
			System.out.println("Who is known as the founder of Java?");
			System.out.println("a. James Gosling\nb. E.K. Brukle");
			uldine("a", "a. ja b. ,  ega sa kahest 50/50 ei saa votta");
		}
	}
	
	public static void opingukorraldus() {
		System.out.println("Opingukorraldus ja erialatutvustus!");
		int suva;
		suva = (int) (Math.random() * 4);
		if (suva == 0) {
			System.out.println("Kukkusid eksamil labi. Kaua on voimalik eksamit jarele teha?");
			System.out.println("a. Kordusooritust ei ole\nb. Oigus kordussoorituseks kehtib 3 kuud arvates aine lopetamissemestrist.\nc. Oigus kordussoorituseks kehtib ulejargmise semestri punase joone paevani arvates aine lopetamissemestrist.");
			uldine("c", "b. ja c.");
			
		}
		
		if (suva == 1) {
			System.out.println("Kui palju maksab eksami kordussooritus, kui oled tasulisel (OF) oppekohal?");
			System.out.println("a. Kui opid REV/OF oppekohal, tuleb maksta kordussoorituse tasu 25 � HITSA kontole.\nb. Kui opid REV/OF oppekohal, tuleb maksta kordussoorituse tasu 50 � HITSA kontole.\nc. Kui opid REV/OF oppekohal, tuleb maksta kordussoorituse tasu 20 � HITSA kontole.");
			uldine("c", "a. ja c.");
				 
		}
		if (suva == 2) {
			System.out.println("Kellega kokku leppida, et kordussooritust teha?");
			System.out.println("a. rektoriga\nb. oppeosakonnaga\nc. oppejouga\nd. isaga");
			uldine("c", "b. ja c.");
			
		}
		if (suva == 3) {
			System.out.println("Kui mitme EAP ulatuses tuleb tasuta oppides oppekulud osaliselt h�vitada aasta lopuks, kui esimese semestri lopuks on olemas 21 EAPd ja teise semestri lopuks 28 EAPd?");
			System.out.println("a. 300�\nb. 250�\nc. 200�\nd. 100�");
			uldine("b", "b. ja c.");
		}
	}

	public static void mata() {
		System.out.println("Matemaatiline analuus!");
		int suva;
		suva = (int) (Math.random() * 4);
		if (suva == 0) {
			System.out.println("Milline on y=arccos x maaramispiirkond?");
			System.out.println("a. X=[1;-1]\nb. X=[-1;1]\nc. R");
			uldine("b", "a. ja b.");
		}
		
		if (suva == 1) {
			System.out.println("Milline on y=cos x maaramispiirkond?");
			System.out.println("a. X=[1;-1]\nb. X=[-1;1]\nc. R");
			uldine("c", "b. ja c.");
			
		}
		if (suva == 2) {
			System.out.println("y=sinx tuletise vottes saame");
			System.out.println("a. y'=-sinx\nb. y=tanx\nc. y=cosx\nd. y=-cosx");
			uldine("c", "c. ja d.");
			
		}
		if (suva == 3) {
			System.out.println("Missugune neist on paarisfunktsiooni tunnus?");
			System.out.println("a. f(-x) = -f(x)\nb. 1+f(x) = f(-x)\nc. f(-x) = f(x)\nd. f(x) = f(x)");
			uldine("c", "a. ja c.");
		}
	}
	//Kontrollib sinu vastust kusimusele ja kaitub vastavalt.
	public static void uldine(String oige, String abikors){
		System.out.println("Sul on " + sober + " sopra alles jaanud.");
		String vastus = TextIO.getlnString();
		if (vastus.equals("s") && sober > 0) {
			sober = sober - 1;
			System.out.println("Alles jai " + abikors);
			String vastus2 = TextIO.getlnString();
			if (vastus2.equals(oige)) {
				System.out.println("Tubli vastasid oigesti.");
				punkte = punkte + 1;
			} else {
				System.out.println("Kahjuks see on vale vastus");
				punkte = punkte - 1;
				if(punkte == -4){
					mangLabi();
				}
			}
		}	
		else if (vastus.equals(oige)) {
			System.out.println("Tubli vastasid oigesti.");
			punkte = punkte + 1;
		} else {
			System.out.println("Kahjuks see on vale vastus");
			punkte = punkte - 1;
			if(punkte == -4){
				mangLabi();
			}
		}
	}
	//See meetod otsustab, kas saadakse sober voi mitte.
	public static void sober() {
		//60% voimalus endale sober saada.
		double arv = Math.random();
		if (arv < 0.6){
			sober = sober + 1;
			System.out.println("Leidsid endale uue sobra!!");
		}else{
			System.out.println("Kahjuks ei leidnud sa endale sopra. ;(");
		}
		
	}
	//Viimane meetod, mis annab teada mis tulemus saadi ja sulgeb programmi.
	public static void mangLabi(){
		System.out.println();
		if(punkte == -4){
		System.out.println("Kogusid endale -4 punkti.\nKukkusid koolist valja!!\nProovi jargmine aasta uuesti.");
		System.exit(1);			
		}else if(punkte < 8){
			System.out.println("Kogusid endale vahem kui 8 punkti.\nKukkusid koolist valja!!\nProovi j�rgmine aasta uuesti.");
			System.exit(1);	
		}else if(punkte == 8){
			System.out.println("Kogusid endale 8 punkti.\n Saad keskmiseks hindeks 1, vaga nigel tulemus.");
			System.exit(1);	
		}else if(punkte == 9){
			System.out.println("Kogusid endale 9 punkti.\n Saad keskmiseks hindeks 2, kasin.");
			System.exit(1);	
		}else if(punkte == 10){
			System.out.println("Kogusid endale 10 punkti.\n Saad keskmiseks hindeks 3, rahuldav.");
			System.exit(1);	
		}else if(punkte == 11){
			System.out.println("Kogusid endale 11 punkti.\n Saad keskmiseks hindeks 4, hea.");
			System.exit(1);	
		}else{
			System.out.println("Kogusid endale 12 punkti.\n Saad keskmiseks hindeks 5, suureparane!!");
			System.exit(1);	
		}
		
	}
}